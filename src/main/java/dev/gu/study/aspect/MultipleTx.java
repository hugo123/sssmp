package dev.gu.study.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author guweichao
 * @created_at 2019年5月9日 上午10:51:42
 * @desc 多数据源事务
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface MultipleTx {
	String[] value() default {};
}
