package dev.gu.study.bean.datasource1;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;



@Mapper
@Component
public interface TB1Mapper  extends BaseMapper<Tb1>{
	@Select("select * from tb1")
	List<Tb1> selectAll();
}
