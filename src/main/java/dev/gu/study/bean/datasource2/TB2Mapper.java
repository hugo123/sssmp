package dev.gu.study.bean.datasource2;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;



@Mapper
@Component
public interface TB2Mapper  extends BaseMapper<Tb2>{
	@Select("select * from tb2")
	List<Tb2> selectAll();
}
