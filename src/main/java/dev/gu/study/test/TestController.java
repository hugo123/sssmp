package dev.gu.study.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.gu.study.aspect.MultipleTx;
import dev.gu.study.bean.datasource1.Tb1;
import dev.gu.study.bean.datasource1.TB1Mapper;
import dev.gu.study.bean.datasource2.Tb2;
import dev.gu.study.bean.datasource2.TB2Mapper;

//spring 跨数据库事务 这里的名字就是DataSourceConfig中的TransactionManager的注册名字，表示这些事务一起commit或rollback
@MultipleTx({ "cstTxManager", "iTxManager" })
@RestController
@RequestMapping("/test")
public class TestController {

	@Autowired
	private TB2Mapper tb2m;
	@Autowired
	private TB1Mapper tb1m;

	@GetMapping("/tb1")
	public String testTB1() {
		List<Tb1> tb1s = tb1m.selectAll();
		tb1s.forEach(tb1 -> {
			System.out.println(tb1.getId());
			System.out.println(tb1.getName());
		});
		return tb1s.toString();
	}

	@GetMapping("/tb2")
	public String testTB2() {
		List<Tb2> tb2s = tb2m.selectAll();
		tb2s.forEach(tb -> {
			System.out.println(tb.getId());
			System.out.println(tb.getName());
		});
		return tb2s.toString();
	}

	@GetMapping("/tball")
	public String testAllRead() {
		String tball = "";

		List<Tb1> tb1s = tb1m.selectAll();
		tb1s.forEach(tb1 -> {
			System.out.println(tb1.getId());
			System.out.println(tb1.getName());
		});
		tball += "tb1:" + tb1s.toString();

		List<Tb2> tb2s = tb2m.selectAll();
		tb2s.forEach(tb -> {
			System.out.println(tb.getId());
			System.out.println(tb.getName());
		});
		tball += "\r\ntb2:" + tb2s.toString();
		return tball;
	}

	@GetMapping("/commit")
	public String testCommit() {
		Tb1 tb1 = new Tb1();
		tb1.setName("add.test1.commit");
		tb1m.insert(tb1);

		Tb2 tb2 = new Tb2();
		tb2.setName("add.test2.commit");
		tb2m.insert(tb2);
		return "succ";
	}

	@GetMapping("/rollback")
	public String testRollback1() {
		Tb1 tb1 = new Tb1();
		tb1.setName("add.test1.rollback");
		tb1m.insert(tb1);
		Tb2 tb2 = new Tb2();
		tb2.setName("add.test2.rollback");
		tb2m.insert(tb2);

		//insert成功后会生成自增id，这里判断id生成后 ，两个datasource都进行rollback
		if (tb1.getId() != null && tb2.getId() != null) {
			System.out.println("tb1:"+tb1.getId());
			System.out.println("tb2:"+tb2.getId());
			throw new RuntimeException("rollback if insert success");
		}
		return "rollback";
	}

}
