-- 0.2.5;

DROP
	TABLE
		IF EXISTS `tb2`;

CREATE
	TABLE
		`tb2` ( `id` INT(11) NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(255) NOT NULL COMMENT '属性名',
		PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8;


INSERT INTO tb2(name) value ('ds2.tb2.name');