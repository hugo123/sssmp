-- 0.2.5;

DROP
	TABLE
		IF EXISTS `tb1`;

CREATE
	TABLE
		`tb1` ( `id` INT(11) NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(255) NOT NULL COMMENT '属性名',
		PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO tb1(name) value ('ds1.tb1.name');